package cn.xej;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DubboBootApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboBootApiApplication.class, args);
    }

}
