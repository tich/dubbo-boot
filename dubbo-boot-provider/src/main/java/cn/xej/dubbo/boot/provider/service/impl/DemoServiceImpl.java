package cn.xej.dubbo.boot.provider.service.impl;

import cn.xej.service.DemoService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


@Service(loadbalance = "roundrobin")
@Component
public class DemoServiceImpl implements DemoService {

    @Override
    public String say(String name) {
        return "provider1"+name;
    }
}
