package cn.xej.dubbo.boot.consumer.service.impl;

import cn.xej.dubbo.boot.consumer.service.OrderService;
import cn.xej.service.DemoService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Reference(loadbalance = "roundrobin",url = "dubbo://localhost:20880")
//    @Reference  cn.xej.service.DemoService
    private DemoService demoService;

    @Override
    public String initOrder() {
        return demoService.say("大鹏");
    }
}
