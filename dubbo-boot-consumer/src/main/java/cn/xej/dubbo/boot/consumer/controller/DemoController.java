package cn.xej.dubbo.boot.consumer.controller;

import cn.xej.dubbo.boot.consumer.service.OrderService;
import cn.xej.service.DemoService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Action;

@RestController
public class DemoController {

    @Autowired
    private OrderService orderService;


    @RequestMapping("hello")
    public String hello(){
        return orderService.initOrder();
    }

}
